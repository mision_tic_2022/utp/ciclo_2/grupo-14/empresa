package com.empresa;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Empresa objEmpresa = new Empresa("Tesla", "Cra 100 #55-22", "123456789");
        objEmpresa.menu();
        
        //Carro carro = new Carro("Gris", 200, "2022", "ABC1234");
        //System.out.println(carro);

        //Creación de arreglos
        /* int[] numeros = {10,20,30,40,50,60};
        double decimales[] = new double[6];
        decimales[0] = 10.1;
        decimales[1] = 20.2; */
        //ciclos();
    }

    public static void ciclos(){
        int contador = 0;
        while(contador < 10){
            System.out.println("Iteracion "+contador+" desde dentro de un while");
            contador++;
        }

        System.out.println("***********************************************************");
        contador = 0;
        do{
            System.out.println("Iteracion "+contador+" desde dentro de un do-while");
            contador++;
        }while(contador < 10);

        System.out.println("***********************************************************");
        for(int i = 0; i < 10; i++){
            System.out.println("Iteracion "+i+" desde dentro de un for");
        }
    }

}
