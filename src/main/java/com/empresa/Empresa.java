package com.empresa;

import java.util.Scanner;

public class Empresa {
    //ATRIBUTOS
    private String nombre;
    private String telefono;
    private String direccion;
    private String email;
    private String nit;
    private Carro[] carros;

    //CONSTRUCTORES
    public Empresa(String nombre, String telefono, String direccion, String email, String nit){
        this.nombre = nombre;
        this.telefono = telefono;
        this.direccion = direccion;
        this.email = email;
        this.nit = nit;
        //inicializar arreglo de tamaño 50
        carros = new Carro[50];
    }

    public Empresa(String nombre, String direccion, String nit){
        this.nombre = nombre;
        this.direccion = direccion;
        this.nit = nit;
        this.telefono = "";
        this.email = "";
        //inicializar arreglo de tamaño 50
        carros = new Carro[50];
    }

    public Empresa(){
        inicializar();
    }

    public void inicializar(){
        nombre = "";
        direccion = "";
        telefono = "";
        email = "";
        nit = "";
        //inicializar arreglo de tamaño 50
        carros = new Carro[50];
    }

    //CONSULTORES

    public String getNombre() {
        return nombre;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public String getEmail() {
        return email;
    }

    public String getNit() {
        return nit;
    }

    public Carro[] getCarros(){
        return carros;
    }

    public Carro getCarro(int pos){
        return carros[pos];
    }

    //MODIFICADORES


    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }

    public void setCarro(int pos, Carro carro){
        carros[pos] = carro;
    }

    //ACCIONES
    public void fabricar_carro(String placa, String modelo){
        //Crear objeto de tipo Carro
        Carro carro = new Carro(modelo, placa);
        for(int i = 0; i < this.carros.length; i++){
            if(this.carros[i] == null){
                this.carros[i] = carro;
                break;
            }
        }
    }

    public void fabricar_carro(String color, double vel_maxima, String modelo, String placa){
        Carro carro = new Carro(color, vel_maxima, modelo, placa);
        for(int i = 0; i < this.carros.length; i++){
            if(this.carros[i] == null){
                this.carros[i] = carro;
                break;
            }
        }
    }

    public void solicitar_datos_carro(Scanner leer){
            System.out.print("Ingrese modelo del Carro: ");
            String modelo = leer.nextLine();

            System.out.print("Ingrese la placa del Carro: ");
            String placa = leer.nextLine();

            System.out.print("Ingrese el color: ");
            String color = leer.nextLine();

            System.out.print("Ingrese velocidad maxima que desea para el carro: ");
            double vel_maxima = leer.nextDouble();

            fabricar_carro(color, vel_maxima, modelo, placa);
    }

    public void mostrar_carros(){
        for(int i = 0; i < carros.length; i++){
            if(carros[i] != null){
                System.out.println(carros[i]);
            }
        }
    }

    public void mostrar_carro_x_placa(String placa){
        for(int i = 0; i < carros.length; i++){
            if(carros[i] != null && carros[i].getPlaca().equalsIgnoreCase(placa)){
                System.out.println(carros[i]);
                /*if(carros[i].getPlaca().equalsIgnoreCase(placa)){
                    System.out.println(carros[i]);
                }
                */
            }else{
                System.out.println("No existe un carro con la placa ingresada");
            }
        }
    }

    public void menu(){

        String mensaje = "-------------------------------FABRICA DE CARROS----------------------\n";
        mensaje += "1) Fabricar un carro\n";
        mensaje += "2) Mostrar todos los carros\n";
        mensaje += "3) Mostrar carro por placa\n";
        mensaje += "-1) Salir\n";
        mensaje += ">>> ";

        try(Scanner leer = new Scanner(System.in)){
            int opcion = 0;
            do{
                System.out.print(mensaje);
                opcion = leer.nextInt();
                leer.nextLine();
                //evaluar la opción ingresada
                switch(opcion){
                    case 1:
                        solicitar_datos_carro(leer);
                        break;
                    case 2:
                        mostrar_carros();
                        break;
                    case 3:
                        System.out.print("Por favor ingrese la placa: ");
                        String placa = leer.nextLine();
                        mostrar_carro_x_placa(placa);
                        break;
                    case -1:
                        break;
                    default:
                        System.out.println("Por favor ingrese una opcion valida");
                }

            }while(opcion != -1);

        }catch(Exception error){
            System.err.println(error.getMessage());
        }

    }
}
